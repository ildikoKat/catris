export const STAGE_WIDTH = 12;
export const STAGE_HEIGHT = 20;

export const createStage = () =>
    Array.from(Array(STAGE_HEIGHT), () =>
        new Array(STAGE_WIDTH).fill([0, 'clear'])
    )

export const checkCollision = (player, stage, { x: moveX, y: moveY }) => {
    for (let row = 0; row < player.tetromino.length; row += 1) {
        for (let col = 0; col < player.tetromino[0].length; col += 1) {
            // Check that we are on a tetromino cell
            if (player.tetromino[row][col] !== 0){
                if(
                //Check if move is inside game height (row)
                //Shouldn't go trough bottom
                !stage[row + player.pos.y + moveY] || 
                //Check if move is inside game width (column)
                !stage[row + player.pos.y + moveY][col + player.pos.x + moveX] ||
                //Check the cell we're moving to is clear
                stage[row + player.pos.y + moveY][col + player.pos.x + moveX][1] !== 'clear'
                ){
                    return true;
                }
            }
        }
    }
}
import cat1 from './images/cat1.png'
import I from './images/I.png'
import J from './images/J.png'
import L from './images/L.png'
import O from './images/O.png'
import S from './images/S.png'
import T from './images/T.png'
import Z from './images/Z.png'

export const TETROMINOS = {
    0: { 
        shape: [[0]], 
        color: '255, 255, 204'
    },
    //when we are not showing anything
    I: {
        shape: [
            [0, 'I', 0, 0],
            [0, 'I', 0, 0],
            [0, 'I', 0, 0],
            [0, 'I', 0, 0]
        ],
        color: '80, 227, 230',
        image: I
    },
    J: {
        shape: [
            [0, 'J', 0 ],
            [0, 'J', 0 ],
            ['J', 'J', 0 ]
        ],
        color: '36, 95, 223',
        image: J
    },
    L: {
        shape: [
            [0, 'L', 0 ],
            [0, 'L', 0 ],
            [0, 'L', 'L' ]
        ],
        color: '223, 173, 36',
        image: L
    },
    O: {
        shape: [
            ['O', 'O'],
            ['O', 'O']
        ],
        color: '223, 217, 36',
        image: O
    },
    S: {
        shape: [
            [0, 'S', 'S' ],
            ['S', 'S', 0 ],
            [0, 0, 0]
        ],
        color: '48, 173, 36',
        image: S
    },
    T: {
        shape: [
            ['T', 'T', 'T' ],
            [0, 'T', 0 ],
            [0, 0, 0]
        ],
        color: '48, 0, 36',
        image: T
    },
    Z: {
        shape: [
            ['Z', 'Z', 0 ],
            [0, 'Z', 'Z' ],
            [0, 0, 0]
        ],
        color: '227, 78, 78',
        image: Z
    },
}

export const randomTetromino = () => {
    const tetrominos = 'IJLOSTZ';
    const randTetromino = tetrominos[Math.floor(Math.random() * tetrominos.length)];
    return TETROMINOS[randTetromino];
}
import React from 'react';
import styled from 'styled-components'

import {TETROMINOS} from '../tetrominos'

export const StyledCell = styled.div`
    width: auto;
    /* background: rgba(${props => props.color}, 0.8); */
    background: url(${props => props.image }) rgba(255, 255, 204, 0.8);
    background-size: contain;
    border: ${props => (props.type === 0 ? '0px solid' : '1px solid')};
    /* border: ${props => (props.type === 0 ? '0px solid' : '4px solid')}; */
    /* border-bottom-color: rgba(${props => props.color}, 0.1);
    border-right-color: rgba(${props => props.color}, 1);
    border-top-color: rgba(${props => props.color}, 1);
    border-left-color: rgba(${props => props.color}, 0.3); */
`

const Cell = ({type}) => (
<StyledCell type={type} color={TETROMINOS[type].color} image={TETROMINOS[type].image} >{console.log('rerender')}</StyledCell>
);

export default React.memo(Cell);
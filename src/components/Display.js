import React from 'react';
import styled from 'styled-components';

const StyledDisplay = styled.div`
    box-sizing: border-box;
    display: flex;
    align-items: center;
    margin: 0 0 20px 0;
    padding: 20px;
    border: 4px solid #333;
    min-height: 30px;
    width: 100%;
    border-radius: 20px;
    color: ${props => (props.gameover ? 'red' : '#666666')};
    background-color: rgb(255, 255, 204);
    font-family: Pixel, Arial, Helvetica, sans-serif;
    font-size: 0.8rem;
`

const Display = ({gameOver, text}) =>(
    <StyledDisplay gameover={gameOver}>{text}</StyledDisplay>
)

export default Display;
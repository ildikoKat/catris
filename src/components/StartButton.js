import React from 'react'
import styled from 'styled-components';
import catEye from '../images/catEye.jpg'

const StyledStartButton = styled.div`
    box-sizing: border-box;
    margin: 0 0 20px 0;
    padding: 20px;
    min-height: 30px;
    width: 100%;
    border-radius: 10px;
    color: #202020;
    
    background: url(${catEye});
    background-size: contain;
    background-repeat: no-repeat;

    font-family: Pixel, Arial, Helvetica, sans-serif;
    font-size: 1rem;

    cursor: pointer;
    size: 50%;
`
const StartButton = ({callback}) => (
    <StyledStartButton onClick={callback}> Start </StyledStartButton>
);

export default StartButton;
import React, { useState } from 'react'

// Components
import Stage from './Stage'
import Display from './Display'
import StartButton from './StartButton'

import styled from 'styled-components'
import bgImage from '../images/cat.jpg'

//Custom hooks
import { usePlayer } from '../hooks/usePlayer'
import { useStage } from '../hooks/useStage'
import { useInterval } from '../hooks/useInterval'
import { useGameStatus } from '../hooks/useGameStatus'

import { createStage, checkCollision } from '../gameHelpers'
import { TETROMINOS } from '../tetrominos'


export const StyledTetrisWrapper = styled.div`
    width: 100vw;
    height: 100vh;
    background: url(${bgImage}) #000;
    background-repeat: repeat;
`
export const StyledTetris = styled.div`
    display: flex;
    align-items: flex-start;
    padding: 20px;
    margin: 0 auto;
    max-width: 900px;

    aside{
        width: 100%;
        max-width: 200px;
        display: block;
        padding: 0 20px;
    }
`
const Tetris = () => {

    const [dropTime, setDropTime] = useState(null);
    const [gameOver, setgameOver] = useState(false);

    const [player, updatePlayerPos, resetPlayer, playerRotate] = usePlayer();
    const [stage, setStage, rowsCleared] = useStage(player, resetPlayer);
    const [score, setScore, rows, setRows, level, setLevel] = useGameStatus(rowsCleared);

    console.log("Re-render. Player", player.pos);

    const movePlayer = (dir) => {
        if(!checkCollision(player, stage, {x: dir, y:0})){
         updatePlayerPos({ x: dir, y: 0 });
        }
    }

    const startGame = () => {
        console.log("STARTED");
        setStage(createStage());
        resetPlayer();
        setgameOver(false);
        setDropTime(1000);
        setScore(0);
        setRows(0);
        setLevel(0);
    }

    const drop = () => {
        // increase level when player has cleared 10 rows
        if (rows > (level +1 ) *10){
            setLevel(prev => prev+1);
            // inscrese speed
            setDropTime( 1000 / (level +1) + 200)
        }
        if(!checkCollision(player, stage, {x: 0, y:1})){
            updatePlayerPos({ x: 0, y: 1, collided: false });
        } else{
            //Game over
            if(player.pos.y < 1){
                setgameOver(true);
                setDropTime(null);
            }
            updatePlayerPos({ x: 0, y: 0, collided: true });
        }
    }

    const keyUp = ({keyCode}) => {
        if(!gameOver){
            if(keyCode === 40){
                console.log("INterval on")
                setDropTime(1000 / (level +1) + 200);
            }
        }
    }

    const dropPlayer = () => {
        console.log("INterval off")
        // setDropTime(null);
        drop();
    }

    const move = ({ keyCode }) => {
        if (!gameOver) {
            if (player.tetromino !== TETROMINOS[0].shape) {
                switch (keyCode) {
                    case 37: movePlayer(-1); break; //Left
                    case 39: movePlayer(1); break; //Right
                    case 40: dropPlayer(); break;  //Down - faster
                    case 32: playerRotate(stage, 1); break;// space - rotating
                    case 38: playerRotate(stage, -1); break;//up - reverse rotating
                }
            }
        }
    }

    useInterval(() => drop(), dropTime);

    return (
        <StyledTetrisWrapper role='button' tabIndex='0' onKeyDown={e => move(e)} onKeyUp={e => keyUp(e)}>
            <StyledTetris>
                <Stage stage={stage} />
                <aside>
                    {gameOver ? (
                        <Display gameOver={gameOver} text='Game Over' />
                    ) : (
                            <div>
                                <Display text={`Score: ` + score }/>
                                <Display text={`Rows: ` +rows} />
                                <Display text={`Level: ` + level} />
                            </div>
                        )}
                    <StartButton callback={startGame} />
                </aside>
            </StyledTetris>
        </StyledTetrisWrapper>
    )
}

export default Tetris;
import { useState, useCallback } from 'react'
import { randomTetromino, TETROMINOS } from '../tetrominos'
import { STAGE_WIDTH, checkCollision } from '../gameHelpers';

export const usePlayer = () => {
    const [player, setPlayer] = useState({
        pos: { x: 0, y: 0 },
        tetromino: TETROMINOS[0].shape,
        collided: false
    });

    const rotate = (tetromino, dir) => {
        //Make rows the cols (transpose)
        const newTetrominoTut = tetromino.map((_, index) =>
            tetromino.map(col => col[index])
        );

        const tLength = tetromino.length;
        let newTetromino = Array.from(Array(tLength), () =>
            new Array(tLength).fill(0)
        );

        for (let i = 0; i < tetromino.length; i++) {
            for (let j = 0; j < tetromino.length; j++) {
                newTetromino[i][j] = tetromino[j][i]
            }
        }

        //Reverse the rows because we need a mirror image
        let rotatedTetromino = Array.from(Array(tLength), () =>
            new Array(tLength).fill(0)
        );
        for (let i = 0; i < tLength; i++) {
            for (let j = 0; j < tLength; j++) {
                rotatedTetromino[i][j] = newTetromino[i][tLength - j - 1]
            }
        }

        if (dir > 0) {
            // return rotatedTetromino;
            return newTetrominoTut.map(row => row.reverse())
        } else {
            return newTetrominoTut.reverse();
        }
    }

    const playerRotate = (stage, dir) => {
        const playerClone = JSON.parse(JSON.stringify(player));
        playerClone.tetromino = rotate(playerClone.tetromino, dir);

        const pos = playerClone.pos.x;
        let offset = 1;
        while (checkCollision(playerClone, stage, { x: 0, y: 0 })) {
            playerClone.pos.x += offset;
            offset = -(offset + (offset > 0 ? 1 : -1));
            if (offset > playerClone.tetromino[0].length) {
                rotate(playerClone.tetromino, -dir);
                playerClone.pos.x = pos;
                return;
            }
        }
        setPlayer(playerClone);
    }

    const updatePlayerPos = ({ x, y, collided }) => {
        setPlayer(prev => ({
            ...prev,
            pos: { x: (prev.pos.x += x), y: (prev.pos.y += y) },
            collided
        }))
    };

    const resetPlayer = useCallback(() => {
        setPlayer({
            pos: { x: STAGE_WIDTH / 2 - 2, y: 0 },
            tetromino: randomTetromino().shape,
            collided: false
        })
    }, [])

    return [player, updatePlayerPos, resetPlayer, playerRotate];
}

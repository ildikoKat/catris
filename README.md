This is a tetris game created with the help of Thomas Weibenfalk's Udemy course to deepen my knowledge about React Hooks and to have a little bit of fun coding. <br />

In the project directory: <br />
Install the dependencies with `npm install` <br />
Start the application in development mode with `npm start`. <br />
Open [http://localhost:3000](http://localhost:3000) to view the game in the browser.

Preview of the game:
![Screenshot Of the game](./screenshots/screenshot1.png)